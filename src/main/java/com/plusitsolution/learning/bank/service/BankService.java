package com.plusitsolution.learning.bank.service;

import com.plusitsolution.learning.bank.domain.Account;
import com.plusitsolution.learning.bank.domain.Customer;
import com.plusitsolution.learning.bank.domain.Employee;
import com.plusitsolution.learning.bank.domain.Transactions;
import org.springframework.stereotype.Service;

import java.lang.reflect.AccessibleObject;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class BankService {


    Map<String, Customer> customerMap = new HashMap<>();
    Map<String, Account> accountMap = new HashMap<>();
    Map<String, Account> closeAccountMap = new HashMap<>();
    List<Transactions> informationDepositList = new ArrayList<>();
    List<Transactions> informationWithdrawList = new ArrayList<>();
    List<Transactions> informationTransferList = new ArrayList<>();
    Map<String, Employee> employeeMap = new HashMap<>();




    public String randomAccount(){
        Random random = new Random();
        int code = random.nextInt(50000);
        return Integer.toString(code);
    }

    public Customer registerCustomer(Customer customer){
        boolean statusCustomer = false;
        Pattern formatIdCard = Pattern.compile("[0-9]{13}"); //Regex check
        Matcher checkIdCard = formatIdCard.matcher(customer.getIdCard());
        if(checkIdCard.matches()){
            for(Map.Entry<String,Customer> checkCustomer:customerMap.entrySet()) {
                if (customer.getIdCard().equals(checkCustomer.getValue().getIdCard())) {
                    statusCustomer = true;
                    break;
                }
            }
            if(!statusCustomer){
                customerMap.put(customer.getIdCard(),customer);
                return customer;
            }else {throw  new IllegalAccessError("There is already information in the system.");}
        }else {throw  new IllegalAccessError("Error :The ID card is invalid.");}
    }
    public Employee registerEmployee(Employee employee){
        boolean statusEmployee = false;
        Pattern formatIdCard = Pattern.compile("[0-9]{13}"); //Regex check
        Matcher checkIdCard = formatIdCard.matcher(employee.getIdCard());
        if(checkIdCard.matches()){
            for(Map.Entry<String,Employee> checkEmployee:employeeMap.entrySet()){
                if(employee.getIdCard().equals(checkEmployee.getValue().getIdCard()) ){
                    statusEmployee = true;
                    break;
                }
            }
            if(!statusEmployee){
                employee.setEmployeeStatus("A1");
                employeeMap.put(employee.getEmployeeID(),employee);
                return employee;
            }else {throw new IllegalAccessError("There is already information in the system.");}
        }else {throw  new IllegalAccessError("Error :The ID card is invalid.");}
    }

    public Account openAccount(String idCard){
        Customer customer = customerMap.get(idCard);
        Set<String> allAccounts = customer.getAccountCustomer();
        String code = randomAccount();
        Account account = new Account(code, customer.getName(), customer.getLastName(), 0);
        accountMap.put(account.getAccountId(), account);
        allAccounts.add(account.getAccountId());
        customer.setAccountCustomer(allAccounts);
        return account;
    }
    public Customer checkCustomer(String idCard){
        Customer customer = customerMap.get(idCard);
        customer.getAccountCustomer();
        return customer;
    }

    public Account deposit(String accountId, double amount){
        Account account = accountMap.get(accountId);
        Transactions saveDeposit = new Transactions();
        if(amount > 0){
            account.setAmountDeposit(amount);
            saveDeposit.setAccountId(account.getAccountId());
            saveDeposit.setName(account.getName());
            saveDeposit.setLastName(account.getLastName());
            saveDeposit.setCategory("deposit");
            saveDeposit.setAmount(amount);
            saveDeposit.setTime(LocalDateTime.now());
            informationDepositList.add(saveDeposit);
            return account;
        }else {throw new IllegalAccessError("Wrong amount");}
    }

    public Account withdraw(String accountId,double amount){
        Account account = accountMap.get(accountId);
        Transactions saveWitdraw = new Transactions();
        if(amount > 0 && account.getAmount() >= amount){
            account.setAmountWithdraw(amount);
            account.setAmountDeposit(amount);
            saveWitdraw.setAccountId(account.getAccountId());
            saveWitdraw.setName(account.getName());
            saveWitdraw.setLastName(account.getLastName());
            saveWitdraw.setCategory("withdraw");
            saveWitdraw.setAmount(amount);
            saveWitdraw.setTime(LocalDateTime.now());
            informationWithdrawList.add(saveWitdraw);
            return account;
        }else{throw new IllegalAccessError("Format  Error");}
    }

    public Account transfer(String accountId,String destinationAccountId,double amount){
        Account account1 = accountMap.get(accountId);
        Account account2 = accountMap.get(destinationAccountId);
        Transactions saveTransfer = new Transactions();
        if(amount <= account1.getAmount()){
            account1.setAmountWithdraw(amount);
            account2.setAmountDeposit(amount);
            saveTransfer.setAccountId(account1.getAccountId());
            saveTransfer.setName(account1.getName());
            saveTransfer.setLastName(account1.getLastName());
            saveTransfer.setCategory("transfer");
            saveTransfer.setAmount(amount);
            saveTransfer.setDestinationAccount(account2.getAccountId());
            saveTransfer.setTime(LocalDateTime.now());
            informationTransferList.add(saveTransfer);
            return account1;
        }else {throw new IllegalAccessError("Not enough money in the account.");}
    }

    public Account closeAccount(String accountId){
        Account account = accountMap.get(accountId);
        closeAccountMap.put(account.getAccountId(),account);
        accountMap.remove(accountId);
        return account;
    }

    public Map<String, Account> getCloseAccountMap() {
        return closeAccountMap;
    }

    public Map<String, Account> getAccountMap() {
        return accountMap;
    }

    public Map<String, Employee> getEmployeeMap() {
        return employeeMap;
    }

    public Map<String, Customer> getCustomerMap() {
        return customerMap;
    }

    public List<Transactions> getInformationDepositList() {
        return informationDepositList;
    }

    public List<Transactions> getInformationWithdrawList() {
        return informationWithdrawList;
    }

    public List<Transactions> getInformationTransferList() {
        return informationTransferList;
    }

}
