package com.plusitsolution.learning.bank.domain;

import java.util.HashSet;
import java.util.Set;

public class Account {
    private static int accountIDRunning = 10001;
    private String accountId;
    private String name;
    private String lastName;
    private double amount;


    public Account(){}
    public Account(String accountId,String name,String lastName , double amount){
        this.accountId = (this.accountIDRunning++) + accountId;
        this.name = name;
        this.lastName = lastName;
        this.amount = amount;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmountDeposit(double amount) {
        this.amount = this.amount + amount;
    }
    public void setAmountWithdraw(double amount) {
        this.amount = this.amount - amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

}
