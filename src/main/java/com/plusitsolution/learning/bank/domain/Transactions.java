package com.plusitsolution.learning.bank.domain;

import java.time.LocalDateTime;

public class Transactions extends Account{
    private String category;
    private LocalDateTime time;
    private String destinationAccount;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

}
