package com.plusitsolution.learning.bank.domain;


import java.util.List;
import java.util.Set;

public class Customer {
    private String name;
    private String lastName;
    private String idCard;
    private String address;
    private Set<String> accountCustomer;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toLowerCase();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName.toLowerCase();
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<String> getAccountCustomer() {
        return accountCustomer;
    }
    public void setAccountCustomer(Set<String> accountCustomer) {
        this.accountCustomer = accountCustomer;
    }
}
