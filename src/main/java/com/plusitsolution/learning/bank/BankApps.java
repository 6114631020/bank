package com.plusitsolution.learning.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.plusitsolution"})
public class BankApps {
    public static void main(String[] args) {
        SpringApplication.run(BankApps.class,args);

    }
}
