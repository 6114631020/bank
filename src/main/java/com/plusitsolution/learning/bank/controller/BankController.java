package com.plusitsolution.learning.bank.controller;


import com.plusitsolution.learning.bank.domain.Account;
import com.plusitsolution.learning.bank.domain.Customer;
import com.plusitsolution.learning.bank.domain.Employee;
import com.plusitsolution.learning.bank.domain.Transactions;
import com.plusitsolution.learning.bank.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.RequestPath;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class BankController {

    @Autowired
    private BankService service;

    @PostMapping(value = "/registerCustomer/{idCard}")
    public Customer registerCustomer(@PathVariable String idCard , @RequestBody Customer customer){
        service.registerCustomer(customer);
        return customer;
    }
    @PostMapping(value = "/registerEmployee/{idCard}")
     public Employee registerEmployee(@PathVariable String idCard,@RequestBody Employee employee){
        service.registerEmployee(employee);
        return employee;
    }
    @PostMapping(value = "/openAccount/{idCard}")
    public Account openAccount(@PathVariable String idCard){
        return service.openAccount(idCard);
    }

    @PostMapping("/deposit")
    public Account deposit(@RequestParam String accountId,@RequestParam double amount){
        return service.deposit(accountId,amount);
    }
    @PostMapping("/withdraw")
    public Account withdraw(@RequestParam String accountId,@RequestParam double amount){
        return service.withdraw(accountId,amount);
    }
    @PostMapping("/transfer")
    public Account transfer(@RequestParam String accountId,@RequestParam String destinationAssountId, @RequestParam double amount){
        return service.transfer(accountId, destinationAssountId, amount);
    }
    @PostMapping("/closeAccount")
    public Account closeAccount(@RequestParam String accountID){
        return service.closeAccount(accountID);
    }
    @GetMapping("/accountInformation")
    public Map<String, Account> getAccountMap(){
        return service.getAccountMap();
    }

    @GetMapping("/employeeInformation")
    public Map<String,Employee>getEmployeeMap(){
        return service.getEmployeeMap();
    }
    @GetMapping("/customerInformation")
    public Map<String,Customer>getCustomerMap(){
        return service.getCustomerMap(); }

    @GetMapping("/depositInformation")
    public List<Transactions> getInformationDepositList(){
        return service.getInformationDepositList();}

    @GetMapping("/withdrawInformation")
    public List<Transactions> getInformationWithdrawList(){
        return service.getInformationWithdrawList();}

    @GetMapping("/transferInformation")
    public List<Transactions> getInformationTransferList(){
        return service.getInformationTransferList();}

    @GetMapping("closeAccountInformation")
    public Map<String,Account> getCloseAccountMap(){
        return service.getCloseAccountMap();}

    @PostMapping("checkAccountCustomer")
    public Customer checkCustomer(@RequestParam String idCard){
        return service.checkCustomer(idCard);
    }
}